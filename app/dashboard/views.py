from flask import render_template, request, redirect, url_for, make_response, abort
from app import db
from flask_login import current_user, login_required
from . import dashboard
from ..models import VRecipients, VMailMaster, VMail, send_base_query, draft_base_query
from .forms import ComposeMailForm, UpdateMailForm
import datetime


@dashboard.route("/", methods=["GET", "POST"])
def homepage():
    return render_template("dashboard/dashboard_base.html")

@dashboard.route("/profile")
def profile():
    return render_template("dashboard/profile_blue.html", breadcrumb=['Profile'])

@dashboard.route("/inbox")
def inbox():
    if request.args.get('ref_id'):
        return redirect(url_for('inbox_mail', ref_id=request.args.get('ref_id')))
    
    inbox = send_base_query.filter(
        VMailMaster.recipient_id == current_user.id,  VMailMaster.hidden_to_recipient == False
    ).all()
    for i in inbox:
        print(i)
    headers = ['From', 'Subject', 'Preview', 'Status', 'Attachments', 'Data', 'Select']
    return render_template("dashboard/mailboxx.html", breadcrumb = ['Inbox'],headers=headers, mails=inbox, back_f='dashboard.inbox')


@dashboard.route("/inbox/mail/<int:ref_id>", methods=["GET", "POST"])
def inbox_mail(ref_id):
    inbox_mail = db.session.query(VMailMaster).get_or_404(ref_id)
    if inbox_mail.recipient_id != current_user.id:
        abort(403)
    form = UpdateMailForm()
    if form.validate_on_submit(): 
        if form.mark_read.data:
            inbox_mail.recipient_read = True
            inbox_mail.mail_opened_time = datetime.datetime.now().time()
            inbox_mail.mail_opened_date = datetime.now().date()
        elif form.revert.data:
            inbox_mail.hidden_to_recipient = False
        elif form.archive.data:
            inbox_mail.hidden_to_recipient = True
        db.session.commit()
        return redirect(url_for("dashboard.homepage"))
    return render_template("dashboard/mail.html", mail=inbox_mail)


@dashboard.route("/inbox/sentmail/<int:ref_id>")
def sendbox_mail(ref_id):
    sent_mail = db.session.query(VMailMaster).get_or_404(ref_id)
    if sent_mail.author_prsnl_id != current_user.id:
        abort(403)
    return render_template("dashboard/mail.html", mail=sent_mail) 
    

@dashboard.route("/archive")
def archive():
    archive = send_base_query.filter(
        VMailMaster.recipient_id == current_user.id, VMailMaster.hidden_to_recipient == True
    ).all()
    # return render_template("test-list.html", func_name="archive", items=archive)
    headers = ['To', 'Subject', 'Preview', 'Status', 'Attachments', 'Data', 'Select']
    return render_template("dashboard/mailboxx.html", breadcrumb = ['Archive'], headers=headers, mails=archive)



@dashboard.route("/sendbox/")
@login_required
def sendbox():
    sendbox = send_base_query.filter_by(author_prsnl_id= current_user.id).all()
    headers = ['To', 'Subject', 'Preview', 'Status', 'Attachments', 'Data', 'Select']
    return render_template("dashboard/mailboxx.html", breadcrumb = ['Sendbox'], headers=headers, mails=sendbox)



@dashboard.route("/drafts/")
@login_required
def drafts():
    drafts = draft_base_query.filter_by(author_prsnl_id=current_user.id).all()
    headers = ['To', 'Subject', 'Preview', 'Attachments', 'Data', 'Select']
    return render_template("dashboard/mailboxx.html", breadcrumb = ['Drafts'], headers=headers, mails=drafts)


@dashboard.route("/new_mail", methods=["GET", "POST"])
def compose():
    form = ComposeMailForm()
    if form.validate_on_submit():
        mail = VMail(ml_priority = form.priority.data,
                    ml_sender = current_user.id,
                    ml_subject = form.subject.data,
                    ml_synopsis = form.synopsys.data,
                    ml_content = form.content.data,
                    ml_status = form.send.data
        )
        
        db.session.add(mail)
        db.session.flush()
        
        #TODO count recipients with helper func
        recipient = VRecipients(ml_id = mail.ml_id, 
                                  recipient_id = form.recipient_id.data
                                  )
        
        db.session.add(recipient)
        db.session.commit()

        return redirect(url_for('dashboard.sendbox'))
    return render_template("dashboard/compose.html", form=form, mail_type='New Mail')


@dashboard.route("/drafts/edit/<int:ref_id>/<int:ml_id>", methods=["GET", "POST"])
@login_required
def edit_draft(ref_id, ml_id):
   
    # draft = draft_base_query.filter(VMail.ml_id == ml_id ).first()
    draft = db.session.query(VMail).filter(VMail.ml_id == ml_id).first()
    if draft.ml_sender != current_user.id:
        abort(403)
    form = ComposeMailForm()
    edited = VMail()
    if request.method =='POST':
        if form.validate_on_submit():
            if form.send.data:
                draft.ml_status = True
            elif form.cancel.data:
                draft.delete()
            db.session.commit()
            print('successfully')
            return redirect(url_for("dashboard.homepage"))
    elif request.method == "GET":
        # form.recipient_id.data = draft.recipient_id
        form.subject.data = draft.ml_subject
        form.priority.data = draft.ml_priority
        form.synopsys.data = draft.ml_synopsis
        form.content.data = draft.ml_content
    return render_template("dashboard/compose.html", form=form, mail_type='Draft')


@dashboard.route('/open/')
def open_mail():
    mail_type  = request.args.get('mail_type')
    ref_id = request.args.get('ref_id')
    ml_id = request.args.get('ml_id')

    if mail_type == 'Drafts':
        return redirect (url_for('dashboard.edit_draft', ref_id=ref_id, ml_id=ml_id))
    elif mail_type == 'Sendbox':
        return redirect(url_for('dashboard.sendbox_mail', ref_id=ref_id))
    else:
        return redirect(url_for('dashboard.inbox_mail', ref_id=ref_id))
