from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import (
    IntegerField,
    PasswordField,
    SubmitField,
    BooleanField,
    StringField,
    TextAreaField,
    SelectField
)
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import (
    DataRequired,
    Length,
    Email,
    EqualTo,
    ValidationError,
    email_validator,
    NumberRange
)

from app.models import VPersonnel as user


class LoginForm(FlaskForm):
    id = IntegerField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    remember = BooleanField("Remember Me")
    submit = SubmitField("Login")



    
    