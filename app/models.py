from app import db, login_manager
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import bindparam
from sqlalchemy.ext import baked
from flask_login import UserMixin
import datetime

Base = automap_base()


@login_manager.user_loader
def load_user(id):
    return db.session.query(VPersonnel).get(int(id))


class VPersonnel(Base, UserMixin):
    __tablename__ = "v_personnel"

    id = db.Column("prsnl_id", db.Integer, primary_key=True)
    prsnl_name = db.Column(db.String(60), nullable=False)
    prsnl_last_name = db.Column(db.String(60), nullable=False)
    prsnl_cell = db.Column(db.String)
    prsnl_email = db.Column(db.String)
    prsnl_password = db.Column(db.Text)

    def __repr__(self):
        return f"VPersonnel('{self.id}', '{self.prsnl_email}')"

class VMail(Base):
    __tablename__ = "v_mail"
    ml_id = db.Column('ml_id', db.Integer, autoincrement=True, primary_key=True)
    ml_priority = db.Column('ml_priority', db.String)
    ml_sender = db.Column('ml_sender', db.SmallInteger)
    ml_subject = db.Column('ml_subject', db.String)
    ml_synopsis = db.Column('ml_synopsis', db.String(50))
    ml_content = db.Column('ml_content', db.Text)
    ml_out_approve = db.Column('ml_out_approve', db.Boolean)
    ml_status = db.Column('ml_status', db.Boolean)
    ml_composed_time = db.Column('ml_composed_time', db.Time, default=datetime.datetime.now().time())
    ml_composed_date = db.Column('ml_composed_date', db.Date, default=datetime.datetime.now().date())
    ml_attachment = db.Column('ml_attachment', db.LargeBinary)

class VRecipients(Base):
    __tablename__ = "v_mail_recipients"

    ref_id = ref_id = db.Column('ref_id', db.Integer , autoincrement=True, primary_key=True)
    ml_id = db.Column('ml_id', db.Integer)
    recipient_id = db.Column('recipient_id', db.Integer)
    ref_read = db.Column('ref_read', db.Boolean, default=False)
    opened_on_time = db.Column('opened_on_time', db.Time, nullable=True)
    opened_on_date = db.Column('opened_on_date', db.Date, nullable=True)
    ref_hidden = db.Column('ref_hidden', db.Boolean, default=False)

class VMailMaster(Base):
    __tablename__ = "v_mail_full"

    reference_id = db.Column("reference_id", db.Integer, primary_key=True)
    mail_id = db.Column("mail_id", db.Integer)
    author_prsnl_id = db.Column("author_prsnl_id", db.Integer)
    author_name = db.Column("author_name", db.String(60))
    author_last_name = db.Column("author_last_name", db.String(60))
    mail_priority = db.Column("mail_priority", db.String)
    mail_subject = db.Column("mail_subject", db.String)
    mail_synopsys = db.Column("mail_synopsys", db.String(50))
    mail_content = db.Column("mail_content", db.Text)
    mail_out_approval = db.Column("mail_out_approval", db.Boolean)
    author_action = db.Column("author_action", db.Boolean)
    mail_composed_time = db.Column("mail_composed_time", db.Time)
    mail_composed_date = db.Column("mail_composed_date", db.Date)
    mail_attachments = db.Column("mail_attachments", db.LargeBinary)
    recipient_id = db.Column("recipient_id", db.Integer)
    recipient_name = db.Column("recipient_name", db.String(60))
    recipient_last_name = db.Column("recipient_last_name", db.String(60))
    hidden_to_recipient = db.Column("hidden_to_recipient", db.Boolean)
    recipient_read = db.Column("recipient_read", db.Boolean)
    mail_opened_time = db.Column("mail_opened_time", db.Time)
    mail_opened_date = db.Column("mail_opened_date", db.Date)

Base.prepare(db.engine, reflect=True)

# automap for CRUD operations

Mail = Base.classes.t_mail
MailRecipients = Base.classes.t_mail_recipients


# reflections as sources for tables in view
personnel = db.Table("t_personnel", db.metadata, autoload=True, autoload_with=db.engine)
v_inbox = db.Table("v_inbox", db.metadata, autoload=True, autoload_with=db.engine)
v_drafts = db.Table("v_drafts", db.metadata, autoload=True, autoload_with=db.engine)

#global query object
send_base_query = db.session.query(VMailMaster).filter_by(author_action = True)
draft_base_query = db.session.query(VMailMaster).filter_by(author_action = False)
