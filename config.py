from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))


class Config:

    SECRET_KEY = environ.get("SECRET_KEY", default="S#perS3crEt_007")

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    TESTING = environ.get("TESTING", default=True)

    DEBUG = environ.get("DEBUG", default=True)

    PAGINATION_NUM = environ.get("PAGINATION_NUM", default=7)

    # PostgreSQL database
    SQLALCHEMY_DATABASE_URI = "{}://{}:{}@{}:{}/{}".format(
        environ.get("DB_ENGINE", default="postgresql"),
        environ.get("DB_USERNAME", default="elahe"),
        environ.get("DB_PASS", default="397397"),
        environ.get("DB_HOST", default="localhost"),
        environ.get("DB_PORT", default=5432),
        environ.get("DB_NAME", default="db_view_based_lazy"),
    )


class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False
